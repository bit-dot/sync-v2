import argparse
__version__ = "Beta release: Native 0.2"


def main():
    base = argparse.ArgumentParser(description="Sync user command line",
                                   prog="Sync V2")
    base.add_argument("-v", "--version", action="version", version=__version__)

    webchat = base.add_argument_group("webchat")
    webchat.add_argument("--webchat",
                         help="displays unread messages",
                         action="store_true")
    webchat.add_argument("-s",
                         "--safe",
                         help="run safely, minus value will be changed",
                         type=int,
                         metavar="DELAY")

    db_args = base.add_argument_group("database")
    db_args.add_argument("--migrate",
                         help="create database table",
                         action="store_true")
    db_args.add_argument("--drop",
                         help="drop database table",
                         action="store_true")

    user_args = base.add_argument_group("user")
    user_args.add_argument("--insert",
                           help="insert user to database",
                           metavar="USERNAME")
    user_args.add_argument("--delete",
                           help="delete user from database and remove session",
                           metavar="USERNAME")
    user_args.add_argument("--list",
                           help="shows all users with sessions still alive",
                           action="store_true")
    user_args.add_argument("--path",
                           help="webchat.log path",
                           metavar="/root/path")

    args = base.parse_args()

    if args.migrate:
        from db import create_tables

        fn = create_tables()
        for msg in fn:
            print("- \u001b[32m%s\u001b[0m" % msg)

        print("\nTables success created")

    if args.drop:
        from db import drop_tables

        fn = drop_tables()
        for msg in fn:
            print("- \u001b[32m%s\u001b[0m" % msg)

        print("\nTables success dropped")

    if args.webchat:
        from webchat import finds

        finds(safe=args.safe, path=args.path)

    if args.insert:
        from users import input_user
        from getpass import getpass

        sec_passwd = getpass(prompt="Password: ")
        fn = input_user(username=args.insert, password=sec_passwd)

    if args.delete:
        from users import delete_user

        fn = delete_user(username=args.delete)
        for msg in fn:
            print("- \u001b[32m%s\u001b[0m" % msg)

    if args.list:
        from users import list_user
        list_user()
