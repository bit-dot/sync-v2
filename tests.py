import unittest
from users import input_user, delete_user
from webchat import find_chat, find_shop, create_signature
from model import database, Users, APIName, WebChat
from pprint import pp, pprint, PrettyPrinter
import db
import json


class Tests(unittest.TestCase):
    def setUp(self):
        self.username = 'aliffatul18'
        self.password = 'WR0ngW@y'

        self.sign = input_user(username=self.username, password=self.password)
        self.assertTrue(self.sign)

        self.users = Users.select().where(Users.status)
        self.assertTrue(self.users)

    def test_user_create_delete(self):
        user = Users.select().where(Users.status)
        for e in user:
            self.assertIsNotNone(e.shop_id)
            print('test user id : {}'.format(e.shop_id))

    def test_find_chat(self):
        users = Users.select().where(Users.status)

        for e in users:
            result = find_chat(e.token).json()

            self.assertEqual(dict, type(result))

    def test_find_shop(self):
        users = Users.select().where(Users.status)
        key = "91d48f823a5a6d923bdf4aa60d478eb3ef494dee8f8f8874d413531f9dffd634"

        for e in users:
            result = find_shop(key=key, shopid=e.shop_id).json()

            self.assertEqual(dict, type(result))

    def tearDown(self):
        delete_user(username='aliffatul18')


if __name__ == "__main__":
    unittest.main()