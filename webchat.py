from db import database, Users, WebChat
from concurrent.futures import ThreadPoolExecutor
import requests
import json
import calendar
import time
import hmac
import hashlib
import threading
import signal
import sys
import os
import logging


def create_signature(url, key, msg):
    """Generate an authorization key"""

    message = "%s|%s" % (url, json.dumps(msg))
    return hmac.new(bytes(key, encoding="utf-8"),
                    bytes(message, encoding="utf-8"),
                    hashlib.sha256).hexdigest()


def find_shop(key, shopid, **kwargs):
    """Search for a store based on parameters"""

    url = "https://partner.shopeemobile.com/api/v1/shop/get"
    timestamp = calendar.timegm(time.gmtime())

    body = {"partner_id": 840480, "shopid": shopid, "timestamp": timestamp}
    headers = {
        "Content-Type": "application/json",
        "Authorization": create_signature(url=url, key=key, msg=body)
    }

    return requests.post(url, json=body, headers=headers)


def find_chat(token):
    """Search for messages based on parameters"""

    url = "https://seller.shopee.co.id/webchat/api/v1.1/user/sync"
    headers = {
        "Authorization": "Bearer {token}".format(token=token),
        "Connection": "keep-alive",
        "Content-Type": "application/json; charset=utf-8",
        "Referer": "https://seller.shopee.co.id/webchat/conversations",
        "Accept": "*/*",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.5"
    }

    return requests.post(url, headers=headers)


def finds(safe=None, path=None, **kwargs):
    key = "91d48f823a5a6d923bdf4aa60d478eb3ef494dee8f8f8874d413531f9dffd634"
    users = Users.select().where(Users.status)
    path_loc = "{path}/public/webchat.log".format(path=path)

    sys.exit("User not found") if len(users) == 0 else None
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    event = threading.Event()
    while not event.wait(False):
        ACCUMULATOR = []
        TIMEOUT = 60

        os.system("clear") if os.name == "posix" else None
        for usr in users:
            try:
                with ThreadPoolExecutor(max_workers=2) as executor:
                    exec_chat = executor.submit(find_chat, token=usr.token)
                    if usr.shop_name == None:
                        exec_shop = executor.submit(find_shop,
                                                    key=key,
                                                    shopid=usr.shop_id)

                    else:
                        exec_shop = None

                chat = exec_chat.result().json()
                if exec_shop != None:
                    shop = exec_shop.result().json()
                    usr.shop_name = shop["shop_name"]
                    usr.save()

                ACCUMULATOR.append({
                    "name": usr.shop_name,
                    "shopid": usr.shop_id,
                    "unread_count": chat["unread_count"]
                })
            except Exception:
                finds(path=path)  # Reload

        if not os.path.exists(path_loc):
            os.makedirs("{path}/public/".format(path=path))

        logging.basicConfig(filename=path_loc,
                            filemode="a",
                            format="%(asctime)s - %(message)s",
                            level=logging.INFO)
        for e in ACCUMULATOR:
            message = "Name\t: {name}\tShopID\t: {shopid}\
            \tUnread\t: {unread_count}".format(**e)

            with database.atomic() as transaction:
                try:
                    WebChat.insert({
                        "shop_name": e["name"],
                        "key": e["unread_count"]
                    }).execute()
                except Exception:
                    transaction.rollback()
                    sys.exit("Error saving data")

            logging.info(message)

        if safe:
            print(" Mode : safe with {time} delay".format(time=safe))
        while TIMEOUT != 0:
            TIMEOUT -= 1
            print(" Countdown : {:<3}".format(TIMEOUT, ), end="\r")
            time.sleep(1)
