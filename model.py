from peewee import (SqliteDatabase, IntegerField, CharField, DateTimeField,
                    TextField, Model, BooleanField, PostgresqlDatabase)
from datetime import datetime
from dotenv import load_dotenv
import os

load = load_dotenv()

if load:
    db_type = str.upper(os.getenv('db_type') or 'SQLITE')
    if (db_type == None or db_type == '') or db_type == 'SQLITE':
        database = SqliteDatabase('database.sqlite3')

    if db_type == 'POSTGRE':
        database = PostgresqlDatabase(os.getenv('db_database'),
                                      user=os.getenv('db_username'),
                                      password=os.getenv('db_password'),
                                      host=os.getenv('db_host'),
                                      port=5432)


class BaseModel(Model):
    class Meta:
        database = database


class Users(BaseModel):
    user_id = IntegerField()
    username = CharField()
    password = CharField()
    shop_name = CharField(null=True)
    shop_id = IntegerField()
    token = TextField()
    email = CharField()
    uuid = CharField()
    status = BooleanField()


class APIName(BaseModel):
    name = CharField(unique=True)
    url = CharField()


class WebChat(BaseModel):
    shop_name = CharField()
    key = IntegerField()
    timestamp = DateTimeField(default=datetime.now())
