from model import database, Users, APIName, WebChat
import sys
import peewee


def create_tables():
    """Automatically create the required tables in the database"""

    if database.connect():

        database.create_tables([Users, APIName, WebChat])
        yield 'Success created tables'

        with database.atomic() as transaction:
            try:
                APIName.insert_many([{
                    'name':
                    'Login',
                    'url':
                    'https://seller.shopee.co.id/api/v2/login'
                }, {
                    'name':
                    'Sessions',
                    'url':
                    'https://seller.shopee.co.id/webchat/api/v1/sessions'
                }, {
                    'name':
                    'Delete',
                    'url':
                    'https://seller.shopee.co.id/webchat/api/v1/session'
                }, {
                    'name':
                    'ShopInfoPartner',
                    'url':
                    'https://partner.shopeemobile.com/api/v1/shop/get'
                }, {
                    'name':
                    'WebChat',
                    'url':
                    'https://seller.shopee.co.id/webchat/api/v1.1/user/sync'
                }]).execute()

                yield 'Success inserted'
            except peewee.IntegrityError:
                transaction.rollback()
                sys.exit(
                    '\n\u001b[31mAn error occurred in the database\u001b[0m')


def drop_tables():
    """Delete tables in the database automatically"""

    MODEL = (Users, APIName, WebChat)

    try:
        yield 'Dropping tables'
        database.drop_tables(MODEL)

    except peewee.IntegrityError:
        raise Exception('Drop table error')
